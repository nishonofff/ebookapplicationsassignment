package com.example.ebook_applications_assignment.di

import com.example.ebook_applications_assignment.data.repository.RepositoryImpl
import com.example.ebook_applications_assignment.ui.presenter.MapsPresenter
import org.koin.dsl.module

val presentationModule = module {
    factory { RepositoryImpl(get(), get()) }

    factory { MapsPresenter(get())}
}