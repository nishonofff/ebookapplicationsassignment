package com.example.ebook_applications_assignment.di

import android.content.Context
import com.example.ebook_applications_assignment.components.AppConstants.Companion.BASE_API_URL
import com.example.ebook_applications_assignment.components.AppConstants.Companion.CONNECT_TIMEOUT
import com.example.ebook_applications_assignment.components.AppConstants.Companion.READ_TIMEOUT
import com.example.ebook_applications_assignment.components.AppConstants.Companion.WRITE_TIMEOUT
import com.example.ebook_applications_assignment.data.network.ApiService
import com.example.ebook_applications_assignment.data.repository.Repository
import com.example.ebook_applications_assignment.data.repository.RepositoryImpl
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.readystatesoftware.chuck.ChuckInterceptor
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {

    single { createService(get()) }

    single { createRepository(get(), get()) }

    single { createRetrofit(get()) }

    single { createOkHttpClient(get()) }

    single { createMoshiConverterFactory() }

    single { createMoshi() }
}

fun createOkHttpClient(context: Context): OkHttpClient {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    val chuckInterceptor = ChuckInterceptor(context)
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
    return OkHttpClient.Builder()
        .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
        .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
        .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
        .addInterceptor(httpLoggingInterceptor)
        .addInterceptor(chuckInterceptor)
        .build()
}

fun createRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BASE_API_URL)
        .client(okHttpClient)
        .addConverterFactory(MoshiConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
}

fun createMoshi(): Moshi {
    return Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()
}

fun createMoshiConverterFactory(): MoshiConverterFactory {
    return MoshiConverterFactory.create()
}

fun createService(retrofit: Retrofit): ApiService {
    return retrofit.create(ApiService::class.java)
}

fun createRepository(
    apiService: ApiService,
    context: Context
): Repository {
    return RepositoryImpl(apiService, context)
}
