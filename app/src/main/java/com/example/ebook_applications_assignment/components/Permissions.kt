package com.example.ebook_applications_assignment.components

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.app.ActivityCompat

/**
 * @author Ikromjon Nishonofff
 * @since 2019
 */

/**
 * Checks whether the permission is granted or not
 */
fun checkPermissionGranted(context: Context, permission: String): Boolean =
    ActivityCompat.checkSelfPermission(
        context,
        permission
    ) == PackageManager.PERMISSION_GRANTED

/**
 * Request runtime permission
 */
fun requestPermission(
    activity: Activity,
    listPermission: List<String>,
    requestPermissionCode: Int
) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        activity.requestPermissions(
            listPermission.toTypedArray(),
            requestPermissionCode
        )
    }
}
