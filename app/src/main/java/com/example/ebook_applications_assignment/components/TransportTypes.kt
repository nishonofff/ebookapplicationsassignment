package com.example.ebook_applications_assignment.components

enum class TransportTypes(val value: String) {
    DRIVING("driving"),
    WALKING("walking"),
    BICYCLING("bicycling")
}
