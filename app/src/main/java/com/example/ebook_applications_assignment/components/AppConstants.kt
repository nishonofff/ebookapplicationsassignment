package com.example.ebook_applications_assignment.components

interface AppConstants {
    companion object {
        /**
         * Network timeouts
         */
        const val CONNECT_TIMEOUT: Long = 120
        const val WRITE_TIMEOUT: Long = 120
        const val READ_TIMEOUT: Long = 120

        const val BASE_API_URL = "https://maps.googleapis.com/maps/api/distancematrix/"

        const val MEASUREMENT_UNIT_TYPE = "imperial"
    }
}