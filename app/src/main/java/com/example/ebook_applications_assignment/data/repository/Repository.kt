package com.example.ebook_applications_assignment.data.repository

import com.example.ebook_applications_assignment.data.model.DetailsResponse
import com.google.android.gms.maps.model.LatLng
import okhttp3.RequestBody

interface Repository {

    /**
     * Check internet connection
     */
    fun noConnection(): Boolean

    suspend fun getInfo(
        transportType: String,
        origins: String,
        destinations: String
    ): DetailsResponse

}