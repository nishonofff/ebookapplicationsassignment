package com.example.ebook_applications_assignment.data.repository

import android.content.Context
import android.net.ConnectivityManager
import com.example.ebook_applications_assignment.R
import com.example.ebook_applications_assignment.components.AppConstants.Companion.MEASUREMENT_UNIT_TYPE
import com.example.ebook_applications_assignment.data.model.DetailsResponse
import com.example.ebook_applications_assignment.data.network.ApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class RepositoryImpl(
    private val apiService: ApiService,
    private val context: Context
) : Repository {

    override fun noConnection(): Boolean {
        val cm = context.getSystemService(
            Context.CONNECTIVITY_SERVICE
        ) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        if (activeNetwork != null) {
            if (activeNetwork.type == ConnectivityManager.TYPE_WIFI)
                return !activeNetwork.isConnected
            if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE)
                return !activeNetwork.isConnected
        }
        return true
    }

    override suspend fun getInfo(
        transportType: String,
        origins: String,
        destinations: String
    ): DetailsResponse {
        return withContext(Dispatchers.IO) {
            return@withContext apiService.getInfo(
                transportType, origins, destinations,
                MEASUREMENT_UNIT_TYPE,
                context.resources.getString(R.string.map_key)
            )
        }
    }
}