package com.example.ebook_applications_assignment.data.network

import com.example.ebook_applications_assignment.data.model.DetailsResponse
import retrofit2.http.*

interface ApiService {

    @Headers("Accept: application/json")
    @GET("json")
    suspend fun getInfo(
        @Query("mode") transportType: String,
        @Query("origins") origins: String,
        @Query("destinations") destinations: String,
        @Query("units") unitType: String,
        @Query("key") apiKey: String
    ): DetailsResponse
}