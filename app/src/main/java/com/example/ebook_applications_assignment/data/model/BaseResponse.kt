package com.example.ebook_applications_assignment.data.model

import com.squareup.moshi.Json

open class BaseResponse {
    @Json(name = "status")
    var status = ""
    @Json(name = "origin_addresses")
    var originList: List<String>? = null
    @Json(name = "destination_addresses")
    var destinationList: List<String>? = null
}