package com.example.ebook_applications_assignment.data.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
open class DetailsResponse : BaseResponse() {
    var rows: List<Elements>? = null
}

@JsonClass(generateAdapter = true)
data class Elements(
    var elements: List<ElementItem>? = null
)

@JsonClass(generateAdapter = true)
data class ElementItem(
    val status: String,
    val duration: Duration?=null,
    val distance: Distance?=null
)

@JsonClass(generateAdapter = true)
data class Duration(
    val value: Long,
    val text: String
)

@JsonClass(generateAdapter = true)
data class Distance(
    val value: Long,
    val text: String
)