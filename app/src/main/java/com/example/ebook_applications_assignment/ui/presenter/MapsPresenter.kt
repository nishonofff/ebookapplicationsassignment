package com.example.ebook_applications_assignment.ui.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.ebook_applications_assignment.components.TransportTypes
import com.example.ebook_applications_assignment.data.repository.RepositoryImpl
import com.example.ebook_applications_assignment.ui.view.MapsView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent

@InjectViewState
class MapsPresenter(
    private var repositoryImpl: RepositoryImpl
) : MvpPresenter<MapsView>(), KoinComponent {

    private var scope = CoroutineScope(
        Job() + Dispatchers.Main
    )

    fun getDetails(
        transportType: TransportTypes,
        startLatitude: Double,
        startLongitude: Double,
        endLatitude: Double,
        endLongitude: Double
    ) {
        if (repositoryImpl.noConnection()) {
            viewState.showNoInternet()
            return
        }
        viewState.showProgress()

        val origins = "$startLatitude,$startLongitude"
        val destinations = "$endLatitude,$endLongitude"

        scope.launch {
            try {
                val data = repositoryImpl.getInfo(
                    transportType.value,
                    origins,
                    destinations
                )

                when (data.status) {
                    "OK" -> viewState.setData(data)
                    else -> viewState.showError()
                }

                viewState.hideProgress()

            } catch (e: Exception) {
                viewState.showError()
                viewState.hideProgress()
            }
        }
    }
}
