package com.example.ebook_applications_assignment.ui.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.ebook_applications_assignment.R
import com.example.ebook_applications_assignment.utils.AppUtils
import com.example.ebook_applications_assignment.ui.maps.MapsActivity
import kotlinx.android.synthetic.main.activity_main.*

/**
 * @author Ikromjon Nishonofff
 * @since 2019
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        AppUtils.setFullscreen(this)

        btnOpenMap.setOnClickListener { startActivity(Intent(this, MapsActivity::class.java)) }
    }
}
