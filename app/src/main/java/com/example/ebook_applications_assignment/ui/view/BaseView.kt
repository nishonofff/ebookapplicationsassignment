package com.example.ebook_applications_assignment.ui.view

import com.arellomobile.mvp.MvpView

interface BaseView : MvpView {

    fun showNoInternet()

    fun showProgress()

    fun hideProgress()

    fun showError()

}
