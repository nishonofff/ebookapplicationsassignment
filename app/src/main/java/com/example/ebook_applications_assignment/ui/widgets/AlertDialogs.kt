package com.example.ebook_applications_assignment.ui.widgets

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import com.example.ebook_applications_assignment.R

object AlertDialogs {
    fun noInternet(
        context: Context, positiveListener: DialogInterface.OnClickListener?,
        negativeListener: DialogInterface.OnClickListener?
    ) {
        AlertDialog.Builder(context, R.style.AlertDialog)
            .setCancelable(false)
            .setTitle(context.getString(R.string.txt_title_no_connection))
            .setMessage(context.getString(R.string.txt_msg_no_connection))
            .setPositiveButton(R.string.txt_retry, positiveListener)
            .setNegativeButton(R.string.txt_cancel, negativeListener)
            .show()
    }

    fun error(
        context: Context?, positiveListener: DialogInterface.OnClickListener?, message: String?
    ) {
        AlertDialog.Builder(context!!, R.style.AlertDialog)
            .setCancelable(false)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok, positiveListener)
            .show()
    }
}