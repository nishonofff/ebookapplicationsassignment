package com.example.ebook_applications_assignment.ui.widgets

import android.content.Context
import com.example.ebook_applications_assignment.R
import com.example.ebook_applications_assignment.data.model.DetailsResponse
import kotlinx.android.synthetic.main.dialog_location_info.*

/**
 * @author Ikromjon Nishonofff
 * @since 2019
 */
class LocationInfoDialog(
    context: Context?
) : com.google.android.material.bottomsheet.BottomSheetDialog(
    context!!, R.style.BottomSheetDialogTheme
) {

    private val mDialog: LocationInfoDialog
    var mData: DetailsResponse? = null

    init {
        setContentView(R.layout.dialog_location_info)
        mDialog = this
    }

    override fun show() {
        super.show()

        val originTxt = mData?.originList?.get(0)
        val destinationTxt = mData?.destinationList?.get(0)
        val distance = mData?.rows!![0].elements!![0].distance?.value
        val distanceTxt = mData!!.rows!![0].elements!![0].distance?.text
        val durationTxt = mData!!.rows!![0].elements!![0].duration?.text

        if (distance != null) {
            mDialog.txtDistance.text = distanceTxt

            mDialog.txtOriginTitle.text = originTxt
            mDialog.txtDestinationTitle.text = destinationTxt
            mDialog.txtDuration.text = durationTxt

            var txtTransportType = ""
            if (mDialog.transportTypeDriving.isSelected)
                txtTransportType = context.getString(R.string.txt_transport_type_driving)
            if (mDialog.transportTypeBicycle.isSelected)
                txtTransportType = context.getString(R.string.txt_transport_type_bicycling)
            if (mDialog.transportTypeWalking.isSelected)
                txtTransportType = context.getString(R.string.txt_transport_type_walking)

            mDialog.txtDurationTitle.text = String.format(
                context.getString(R.string.txt_duration_title),
                txtTransportType
            )
            mDialog.txtDistanceTitle.text = String.format(
                context.getString(R.string.txt_distance_title),
                txtTransportType
            )
            mDialog.txtDialogTitle.text = String.format(
                context.getString(R.string.txt_dialog_title),
                txtTransportType
            )
        }
    }

    class Builder(context: Context?) {
        private val mBottomSheetDialog: LocationInfoDialog =
            LocationInfoDialog(context)

        fun show(): LocationInfoDialog {
            mBottomSheetDialog.show()
            return mBottomSheetDialog
        }

        fun build(): LocationInfoDialog {
            mBottomSheetDialog.create()
            return mBottomSheetDialog
        }
    }
}