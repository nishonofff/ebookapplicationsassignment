package com.example.ebook_applications_assignment.ui.maps

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.example.ebook_applications_assignment.R
import com.example.ebook_applications_assignment.utils.AppUtils
import com.example.ebook_applications_assignment.components.TransportTypes
import com.example.ebook_applications_assignment.components.checkPermissionGranted
import com.example.ebook_applications_assignment.components.requestPermission
import com.example.ebook_applications_assignment.data.model.DetailsResponse
import com.example.ebook_applications_assignment.ui.presenter.MapsPresenter
import com.example.ebook_applications_assignment.ui.view.MapsView
import com.example.ebook_applications_assignment.ui.widgets.AlertDialogs
import com.example.ebook_applications_assignment.ui.widgets.LocationInfoDialog
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_maps.*
import kotlinx.android.synthetic.main.dialog_location_info.*
import kotlinx.android.synthetic.main.toolbar_row.*
import org.koin.android.ext.android.get

/**
 * @author Ikromjon Nishonofff
 * @since 2019
 */
class MapsActivity : MvpAppCompatActivity(), MapsView, OnMapReadyCallback,
    GoogleMap.OnMapLongClickListener,
    GoogleMap.OnMarkerClickListener {

    @InjectPresenter
    lateinit var mPresenter: MapsPresenter

    @ProvidePresenter
    fun provide(): MapsPresenter = get()

    private lateinit var mMap: GoogleMap

    private val LOCATION_REQUEST_CODE = 0
    private var mDialog: LocationInfoDialog? = null
    private var mOriginLong: Double = 0.0
    private var mOriginLat: Double = 0.0
    private var mDestinationLong: Double = 0.0
    private var mDestinationLat: Double = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        AppUtils.setFullscreen(this)

        if (!checkPermissionGranted(this, Manifest.permission.ACCESS_FINE_LOCATION)
            || !checkPermissionGranted(this, Manifest.permission.ACCESS_COARSE_LOCATION)
        )
            requestLocationPermission()
        else
            initializeMap()

        btnSatellite.setOnClickListener {
            onSatelliteClicked()
        }

        initLocationInfoDialog()
    }

    private fun onSatelliteClicked() {
        if (mMap.mapType == GoogleMap.MAP_TYPE_NORMAL)
            mMap.mapType = GoogleMap.MAP_TYPE_SATELLITE
        else
            mMap.mapType = GoogleMap.MAP_TYPE_NORMAL
    }

    private fun initializeMap() {
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        toolbarTitle.text = getString(R.string.txt_title_maps)
        btnToolbarBack.setOnClickListener { onBackPressed() }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!

        mMap.isMyLocationEnabled = true

        mMap.setOnMapLongClickListener(this)
        mMap.setOnMarkerClickListener(this)
        mMap.setOnMapClickListener { clearMap() }

        setCurrentLocation()
    }

    /**
     * Map long click, adds marker to the selected place
     */
    override fun onMapLongClick(point: LatLng?) {
        clearMap()

        mMap.animateCamera(CameraUpdateFactory.newLatLng(point))

        mMap.addMarker(
            MarkerOptions()
                .position(point!!)
                .icon(AppUtils.getBitmapDescriptorFromVector(this, R.drawable.ic_marker))
                .title(getString(R.string.txt_destination))
                .draggable(true)
        )
    }

    /**
     * Map marker click, shows marker info, distance between current location and marker point
     */
    override fun onMarkerClick(marker: Marker?): Boolean {
        mDestinationLong = marker?.position!!.longitude
        mDestinationLat = marker.position.latitude

        mMap.addMarker(
            MarkerOptions()
                .position(LatLng(mOriginLat, mOriginLong))
                .icon(AppUtils.getBitmapDescriptorFromVector(this, R.drawable.ic_marker_selected))
                .title(getString(R.string.txt_destination))
                .draggable(true)
        )

        onDrivingSelected()

        return true
    }

    private fun getJourneyInfo(transportTypes: TransportTypes) {
        mPresenter.getDetails(
            transportTypes,
            mOriginLat,
            mOriginLong,
            mDestinationLat,
            mDestinationLong
        )
    }

    @SuppressLint("MissingPermission")
    fun setCurrentLocation() {
        if (!checkPermissionGranted(this, Manifest.permission.ACCESS_FINE_LOCATION)
            || !checkPermissionGranted(this, Manifest.permission.ACCESS_COARSE_LOCATION)
        )
            requestLocationPermission()
        else {
            val locationManagerCt =
                getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val locationCt = locationManagerCt
                .getLastKnownLocation(LocationManager.GPS_PROVIDER)

            if (locationCt != null) {
                val location = LatLng(locationCt.latitude, locationCt.longitude)

                mMap.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(location, 8.0f)
                )

                mOriginLong = locationCt.longitude
                mOriginLat = locationCt.latitude
            }
        }
    }

    /**
     * Deletes all markers on the map
     */
    private fun clearMap() {
        mMap.clear()
    }

    override fun setData(data: DetailsResponse) {
        mDialog?.mData = data
        mDialog?.show()
    }

    private fun initLocationInfoDialog() {
        mDialog = LocationInfoDialog.Builder(this)
            .build()

        mDialog!!.transportTypeDriving.setOnClickListener {
            onDrivingSelected()
        }
        mDialog!!.transportTypeBicycle.setOnClickListener {
            onBicyclingSelected()
        }
        mDialog!!.transportTypeWalking.setOnClickListener {
            onWalkingSelected()
        }
        mDialog!!.transportTypePublicTransport.setOnClickListener {
            AlertDialogs.error(
                this,
                null,
                getString(R.string.txt_msg_function_not_available)
            )
        }
        mDialog!!.btnDirections.setOnClickListener {
            AlertDialogs.error(
                this,
                null,
                getString(R.string.txt_msg_function_not_available)
            )
        }
    }

    private fun onDrivingSelected() {
        getJourneyInfo(TransportTypes.DRIVING)
        clearTransportTypeSelections()
        mDialog!!.transportTypeDriving.isSelected = true
    }

    private fun onBicyclingSelected() {
        getJourneyInfo(TransportTypes.BICYCLING)
        clearTransportTypeSelections()
        mDialog!!.transportTypeBicycle.isSelected = true
    }

    private fun onWalkingSelected() {
        getJourneyInfo(TransportTypes.WALKING)
        clearTransportTypeSelections()
        mDialog!!.transportTypeWalking.isSelected = true
    }

    private fun clearTransportTypeSelections() {
        mDialog!!.transportTypeDriving.isSelected = false
        mDialog!!.transportTypeBicycle.isSelected = false
        mDialog!!.transportTypeWalking.isSelected = false
    }

    override fun showNoInternet() {
        AlertDialogs.noInternet(
            this,
            DialogInterface.OnClickListener { _: DialogInterface, _: Int ->
                onDrivingSelected()
            }, null
        )
    }

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

    override fun showError() {
        AlertDialogs.error(this, null, getString(R.string.txt_msg_error))
    }

    private fun requestLocationPermission() {
        requestPermission(
            this,
            listOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ), LOCATION_REQUEST_CODE
        )
    }

    /**
     * Runtime permission request result
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == LOCATION_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initializeMap()

            } else {
                Toast.makeText(
                    this,
                    getString(R.string.txt_msg_permission_denied),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
}
