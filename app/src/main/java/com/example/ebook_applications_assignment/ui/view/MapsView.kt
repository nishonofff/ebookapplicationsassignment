package com.example.ebook_applications_assignment.ui.view

import com.example.ebook_applications_assignment.data.model.DetailsResponse

interface MapsView : BaseView {

    fun setData(data: DetailsResponse)

}
