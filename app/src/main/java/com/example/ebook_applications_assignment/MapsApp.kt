package com.example.ebook_applications_assignment

import android.app.Application
import android.content.Context
import com.example.ebook_applications_assignment.di.appComponent
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MapsApp : Application() {

    companion object {
        private lateinit var instance: MapsApp

        fun getAppContext(): Context = instance.applicationContext
    }

    override fun onCreate() {
        instance = this
        super.onCreate()

        startKoin {
            modules(appComponent)
            androidContext(this@MapsApp)
        }
    }
}