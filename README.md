# Google maps app

### Technologies and libraries
* _Kotlin_
* _Google maps_


### Functionals
* _OnMapLongClick_ - add marker to the selected location
* _OnMapClick_ - clear the map, deletes all markers on the map
* _OnMarkerClick_ - show location info dialog and distance between selected and current locations

